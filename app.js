// console.log("Hello World");

let num1 = parseInt(prompt("Provide a number"));
let num2 = parseInt(prompt("Provide another number"));
let totalNum = num1 + num2;
console.log(totalNum);

if (totalNum < 10) {
    console.warn(`${num1 + num2}`)
} else if (totalNum <= 19) {
    alert(`The difference of the two number are ${num1 - num2}`)
} else if (totalNum >= 20 && totalNum <= 29) {
    alert(`The product of the two number are ${num1 * num2}`)
} else {
    alert(`The quotient of two numbers are ${num1 / num2}`);
}

let name = prompt("What is your name?")
let age = prompt("What is your age?")

if (name == "" || name == null || age == "" || age == null) {
    alert(`Are you a time traveler?`)
} else {
    alert(`Hello ${name}. Your age is ${age}.`)
}

function isLegalAge() {
    age >= 18 ? alert("You are of legal age.") : alert("You are not allowed here.")
}

isLegalAge(age)

switch (age) {
    case ('18'):
        alert("You are now allowed to party.")
        break;
    case ('21'):
        alert("You are now part of the adult society.")
        break;
    case ('65'):
        alert("We thank you for your contribution to society.")
        break;
    default:
        alert("Are you sure you're not an alien?")
        break;
}

try {
    forcedError(age);
}

catch (error) {
    console.warn(error.message);
}
finally {
    isLegalAge(age);
}
